Unu
===
(One in Esperanto language)

An online game version of Uno!

https://youtu.be/zoSqVpFoB7o?t=32


---
Installation:
1. Install yarn
2. `git clone https://gitlab.com/WuerfelDev/unu`
3. `cd unu && yarn install`


Single run: `yarn serve`
Testing: `./observe.sh` (restart node automatically when changes are made)



Todo/Bugtracker:
- `git grep FIXME`
- `git grep TODO`
- Major UI Bugs in gecko/firefox. Cant drop cards anywhere
- Handling multiple tabs [in lobby.js and game.js]
- Color selection for wild cards
- User can get other playerIDs from playersUpdated and with changing cookies they can take over that player. Fix: refer to players by shortended IDs (lower risk) or only by their order and username for UX. (need fixing in lobby) **Does the lobby share other players token?**
- lobby: everyone ready except one. One refreshes browser. (he missed the gamestart FIXME). But the one is also listed in the user list in the game!
