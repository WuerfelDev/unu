//onload
document.addEventListener("DOMContentLoaded", function() { 
    
    inGame();
    
    //createDebugCard();createDebugCard();createDebugCard();createDebugCard();createDebugCard();
    
    let discardpile = document.getElementById("discardpile");
    discardpile.addEventListener('dragover', allowdrop);
    discardpile.addEventListener('drop', drop_onpile);
});


const socket = io('/game/'+getGameCode());
let mycards = {};
let cardorder = [];
let rules = {};
let myturn = false;




//XXX not used
socket.on('refresh', inGame);
socket.on('test', msg=>alert(msg));



socket.on('mycards', function(msg) {
    mycards=msg;
    cardorder = Object.keys(msg);
    updateCards();
});


socket.on('gameData', function(message) {
    console.log(message);
    
    // Set player list
    document.getElementById("players").innerHTML = "<ul><li>"+message["players"].map(player => player.name+(player.current?" (current)":"")).join("</li><li>")+"</li></ul>";
    
    // Set discard pile top card
    
    let discardpile = document.getElementById("discardpile");
    createCard(discardpile,message["discard"]);
    
    rules = message["rules"];
    
    myturn=message["players"].find(player=>player.current).usertoken==getCookie("usertoken");
    console.log(myturn);
});


socket.on('gameFinished', async function(message) {
    //TODO display message who won
    await sleep(500); //server has to set up new Lobby() and create new socket
    location.href = "/lobby/"+getGameCode();
    socket.disconnect();
});


socket.on('disconnect', function(message) {
    //TODO block screen and notify about disconnection, add refreshbutton
    console.log("disconnected");
    location.href = "/";
    //TODO for faster debugging add page refresh
});


function inGame(){
    socket.emit("inGame", getCookie("token"));
}


//XXX debug
// Creates basic 0-9 cards for debugging
let idcounter = 0;
function createDebugCard(){
    let colors = ["blue","green","yellow","red"];
    //mycards.push( { number: Math.floor(Math.random() * 10), color: colors[Math.floor(Math.random() * colors.length)], id: "cardid"+idcounter++} );
    mycards["cardid"+idcounter++] = { number: Math.floor(Math.random() * 10), color: colors[Math.floor(Math.random() * colors.length)]};
    updateCards();
}


function updateCards(){
    document.getElementById("mycards").innerHTML = "";
    let c = 0;
    createDroparea(c);
    for(let cardid of cardorder) {
        // create card
        let card = document.createElement('div');
        
        card = createCard(card,mycards[cardid]);
        
        card.id = cardid;
        card.setAttribute("draggable",  true);
        card.addEventListener("dragstart", card_dragstart);
        card.addEventListener("dragend", card_dragend);
        
        document.getElementById("mycards").appendChild(card);
        createDroparea(++c);
    }
}


// Create HTML structure of a card
function createCard(el,carddata){
    el.className = 'card';
    el.dataset.number = carddata["number"];
    el.dataset.color = carddata["color"];
    el.innerHTML = "<span>"+(carddata["type"].startsWith("wild")?"?":carddata["number"])+"</span>"; //Set span to '?' for all wilds
    return el;
}



// Drop functions
function createDroparea(pos){
    let droparea = document.createElement('div');
    droparea.className = "droparea";
    droparea.dataset.pos = pos;
    droparea.addEventListener('dragover', allowdrop);
    droparea.addEventListener('drop', drop_reorder);
    document.getElementById("mycards").appendChild(droparea);
}


function allowdrop(ev){
    //only allow card drop, and not the surrounding droparea, and drawpile
    if(ev.dataTransfer.types[0]=="cardid" && ev.toElement && ev.toElement.classList.contains("dropoption")){
        ev.preventDefault();
        ev.dataTransfer.dropEffect = "move";
        
    }
    
}

function drop_reorder(ev){
     ev.preventDefault();
     let cardid = ev.dataTransfer.getData("cardid");
     let from = document.getElementById(cardid).previousElementSibling.dataset.pos;
     let to = ev.target.dataset.pos;
     to -= from<to?1:0; // get new position after removing card
     let thiscard = cardorder.splice(from,1)[0];
     cardorder.splice(to, 0, thiscard);
     
     updateCards();
}


function drop_onpile(ev){
    ev.preventDefault();
    let cardid =  ev.dataTransfer.getData("cardid");
    cardorder.splice(cardorder.indexOf(cardid), 1); //Remove the selected card from cardorder

    let data = {
        id: cardid.replace(/\D/g,''),
        cardorder: cardorder.map(el=>el.replace(/\D/g,''))
        };
    if(mycards[cardid]["color"]=="wild"){
        //TODO show color selection dialog
        // Select blue as default for now
        data["selected"] = "blue";
    }
    socket.emit("discard", data);
    //Update cards and pile
    updateCards();
}



function card_dragstart(ev){
    //FIXME detects text as drag if we start dragging from a cards
    if(!ev.target || !ev.target.classList || !ev.target.classList.contains("card")) return false;
    

    ev.dataTransfer.setData("cardid", ev.target.id);
    ev.dataTransfer.effectAllowed = "move";
    ev.target.classList.add("notavail");
    
    let dropareas = document.getElementsByClassName("droparea");
    for(da of dropareas){
        if(da.dataset.pos!=ev.target.previousElementSibling.dataset.pos && da.dataset.pos!=ev.target.nextElementSibling.dataset.pos){
            da.classList.add("dropoption");
        }
        
        if(is_drop_allowed(ev.target.dataset)){
            document.getElementById("discardpile").classList.add("dropoption");
        }
    }
}

function is_drop_allowed(card){
    let discard = document.getElementById("discardpile").dataset;
    
    // jump in rule
    //if(!myturn && !rules.jumpin && discard.color==card.color && discard.number==card.number && !card.color=="wild") return false;
    
    if(!myturn){
        return (rules.jumpin && discard.color==card.color && discard.number==card.number && card.color!="wild");
    }
    
    return (discard.color==card.color || discard.number==card.number || card.color=="wild");
}


function card_dragend(ev){
    if(!ev.target || !ev.target.classList || !ev.target.classList.contains("card")) return false;
    
    ev.target.classList.remove("notavail");
    let dropareas = document.getElementsByClassName("droparea");
    for(da of dropareas){
        da.classList.remove("dropoption");
    }
    document.getElementById("discardpile").classList.remove("dropoption");
}




