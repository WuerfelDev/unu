function getGameCode(){
    let p = location.pathname.split("/"); //[0] is always empty
    if(2 in p) return p[2];
    return "";
}


// https://stackoverflow.com/a/15724300/4346956
function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
