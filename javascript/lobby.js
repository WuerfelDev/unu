//onload
document.addEventListener("DOMContentLoaded", function() { 
    
    document.getElementById("inviteLink").innerHTML = document.location.protocol+"//"+document.location.host+"/join/"+getGameCode();
    
});


const socket = io('/lobby/'+getGameCode());


inLobby();

socket.on('refresh', inLobby);


socket.on('playersUpdated', function(message) {
    document.getElementById("players").innerHTML = "<ul><li>"+message.map(player => player.name+(player.ready?" (ready)":"")).join("</li><li>")+"</li></ul>";
});

socket.on('gameStarting', async function(message) {
    await sleep(500); //server has to set up new Game() and create new socket
    location.href = "/game/"+getGameCode();
    socket.disconnect();
});



function inLobby(){
    socket.emit("inLobby", getCookie("token"));
}

function startGame(){ // Button clicked
    socket.emit("ready", getCookie("token"));
    return false;
}

function copyLink(){
    var textArea = document.createElement("textarea");
    textArea.value = document.getElementById("inviteLink").textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("Copy");
    textArea.remove();
    return false;
}
