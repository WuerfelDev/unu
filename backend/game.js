// Organizes Players before game starts

const Player = require('./player.js');
const Card = require('./card.js');

const util = require('./util.js');

module.exports = class {
    
    constructor(callback, gamecode, players){
        this.parent = callback;
        this.gamecode = gamecode;
        this.players = players;
        //FIXME bad randomness
        this.playerorder = Object.keys(this.players).sort(() => 0.5-Math.random());
        this.conIds = {};
        
        this.currentPlayer = 0;
        this.direction = 1;
        
        //TODO set rules by user before game starts. Hardcoded here
        // Never m
        this.rules = {jumpin: true,};
        
        this.killtimer;
        this.isFinished = false;
        
        this.cards = this.createCards();
        this.drawpile = this.spreadCards();
        this.discard = this.cards[this.drawpile.pop()];

        this.createGame(this);
    }
    

    createGame(here){
        console.log("Game '"+this.gamecode+"' created");
        
        const nsp = this.parent.io.of('/game/'+this.gamecode);
        nsp.on('connection', function(socket){
            clearTimeout(here.killtimer);
            
            socket.on('inGame', function(token) {
                here.playerInGame(token, socket);
            });
            
            socket.on('disconnect', () => {
                here.onDisconnect(socket);
            });
        
            socket.on('ready', function(token) {
                here.onReady(token, socket);
            });
            
            socket.on('discard', function(data) {
                here.onDiscard(data, socket);
            });
            
            

        });
    }
    


    playerInGame(token, socket){
        if(token in this.players){
            this.conIds[socket.id] = token;
            this.players[token].inGame = true;
            //socket.nsp.emit("playersUpdated", this.getPlayersList());
            console.log(token+" in game");
            
            this.sendPlayerCards(socket);
            this.sendGameData(socket);
        }
        //FIXME else error handling, could have two tabs opened
    }
    
    
    onDisconnect(socket){
        // if no players kill game after some 20min
        if(!this.isFinished){
            if(socket.id in this.conIds){
                let token = this.conIds[socket.id];
                this.players[token].inGame = false;
                delete this.conIds[socket.id];
                socket.nsp.emit("playersUpdated", this.getPlayersList());
                console.log(token+" disconnected");
            }
            if(Object.keys(this.conIds).length==0){
                console.log("Last player disconnected. Killtimer started");
                this.killtimer = setTimeout((a) => a.kill(), 20*60*1000, this);
            }
        }
    }
    
    
    // Send current state to every user
    //TODO Send direction
    sendGameData(socket){
        let gamedata = {
            discard: this.discard,
            players: this.getPlayersList(),
            rules: this.rules
            };
        socket.nsp.emit("gameData", gamedata);
    }
    
    
    //Send the current cards to the emitting player
    sendPlayerCards(socket){
        let token = this.conIds[socket.id];
        let data = {};
        for(let c of this.players[token].cards){
            data["cardid"+c] = this.cards[c];
        }
        socket.emit("mycards", data);
    }
    
    // Game functions
    nextPlayer(){
        this.currentPlayer += this.direction;
        
        if(this.currentPlayer == this.playerorder.length){
            this.currentPlayer = 0;
        }else if(this.currentPlayer == -1){
            this.currentPlayer = this.playerorder.length-1;
        }
    }
    
    changeDirection(){
        this.direction *= -1;
    }
    
    getPlayersList(){ //TODO make this useful lol 
        let list = [];
        for(let pos in this.playerorder){
            let status = this.players[this.playerorder[pos]].getStatus();
            if(pos == this.currentPlayer) status["current"]=true;
            list.push(status);
        }
        return list;
        //return this.players.map(this.players);
    }
    
    createCards(){
        let cards = [];
        //let cardid = 0;
        let colors = ["blue","green","yellow","red"];
        for(let color of colors){
            // 1x 0 and 2x 1-9
            for(let i=0;i<=9;i+=.5){
                cards.push({type:"regular", color:color, number:Math.ceil(i)});
            }
            
            // 2x each special card
            for(let i=0;i<2;i++){
                
                cards.push({type:"skip", color:color, number:"↷"});
                cards.push({type:"reverse", color:color, number:"⇄"});
                cards.push({type:"draw2", color:color, number:"+2"});
            }
            
            // Wild cards once
            cards.push({type:"wild", color:"wild", number:""});
            cards.push({type:"wilddraw4", color:"wild", number:"+4"});
        }
        return cards;
    }
    
    spreadCards(){
        let cardids = util.shuffle([...this.cards.keys()]);
        for(let token of this.playerorder){
            let playercards = [];
            for(let i=0;i<7;i++){
                playercards.push(cardids.pop());
            }
            this.players[token].cards = playercards;
        }
        return cardids;
    }
    
    
    
    onDiscard(data, socket){
        // Update players handcards
        this.players[this.conIds[socket.id]].cards = data.cardorder;
        let discard = this.cards[data.id];
        discard["id"] = data.id;
        this.discard = discard;
        if("selected" in data && this.cards[data.id]["color"]=="wild"){
            //Wild card played and color sent
            this.discard["color"] = data["selected"];
        }
        
        //TODO validate its the current user or this.rules.jumpin
        // validate user has the card and is it is allowed to be played (but do it above before setting the new discard card ^^)
        
        //TODO user has to draw cards himself or add his own +2 (handle on frontend)
        switch(discard.type){
            case "skip":
                this.nextPlayer();
                break; 
            case "reverse":
                this.changeDirection();
                break;
            case "draw2":
                break;
            case "wilddraw4":
                break;
        }
        
        this.nextPlayer();
        this.sendGameData(socket);
    }
    
    
    kill(){
        clearTimeout(this.killtimer);
        util.killNamespace(this.parent.io, '/game/'+this.gamecode);
        delete this.parent.activeGames[this.gamecode];
        console.log("Killed game "+this.gamecode);
    }
    
    
    
    //XXX debug
    debug(){
        return this.players;
    }
    
}
