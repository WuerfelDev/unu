// Organizes Games

const util = require('./util.js');

const Game = require('./game.js');
const Lobby = require('./lobby.js');

module.exports = class {

    constructor(io){
        this.io = io;
        this.activeGames = {};
        this.activeLobbies = {}; 
    }
    
    joinLobby(gamecode, username){
        if(this.lobbyExists(gamecode)){
            return this.activeLobbies[gamecode].newPlayer(username);
        }
    }
    
    createLobby(){
        let code;
        do{
            code = util.createToken(6, "abcdefghijklmnopqrstuvwxyz0123456789");
        }while(this.lobbyExists(code)||this.gameExists(code))
        this.activeLobbies[code] = new Lobby(this, code);
        return code;
    }
    
    lobbyExists(code){
        return code in this.activeLobbies;
    }
    
    
    gameExists(code){
        return code in this.activeGames;
    }
    
    lobbyToGame(code){
        let players = this.activeLobbies[code].players;
        for(let key in players) {
            if(!players[key].ready) delete players[key].ready;
        }
        this.activeGames[code] = new Game(this, code, players);
        this.activeLobbies[code].kill();
    }
    
    gameToLobby(code){ // Idea: start next/new game with same people / same code
        //TODO implement lol. Create new player based on username and token
        delete this.activeGames[code];
        this.activeLobbies[code] = new Lobby(this, code);
    }
    
    
    //XXX debug
    debug(gamecode=false){
        if(gamecode && this.lobbyExists(gamecode)){
            return this.activeLobbies[gamecode].debug();
        }else if(gamecode && this.gameExists(gamecode)){
            return this.activeGames[gamecode].debug();
        }
        return {
            lobbies: Object.keys(this.activeLobbies),
            games: Object.keys(this.activeGames)
            };
    }

}
