// Organizes Players before game starts

const Player = require('./player.js');

const util = require('./util.js');

module.exports = class {
    
    constructor(callback, gamecode){
        this.parent = callback;
        this.gamecode = gamecode;
        this.players = {};
        this.conIds = {};
        this.killtimer;
        this.hasStarted = false;
    
        this.createLobby(this);
    }
    

    createLobby(here){
        console.log("Lobby '"+this.gamecode+"' created");
        
        const nsp = this.parent.io.of('/lobby/'+this.gamecode);
        nsp.on('connection', function(socket){
            clearTimeout(here.killtimer);
            
            socket.on('inLobby', function(token) {
                here.playerInLobby(token, socket);
            });
            
            socket.on('disconnect', () => {
                here.onDisconnect(socket);
            });
        
            socket.on('ready', function(token) {
                here.onReady(token, socket);
            });

        });
    }
    
    newPlayer(username){ // user created but websocket not yet connected
        let player = new Player(username);
        this.players[player.token] = player;
        return [player.token, player.usertoken];
    }
    



    playerInLobby(token, socket){
        if(token in this.players){
            this.conIds[socket.id] = token;
            this.players[token].inLobby = true;
            socket.nsp.emit("playersUpdated", this.getPlayersList());
            console.log(token+" in lobby");
        }
        //FIXME else error handling, could have two tabs opened
    }
    
    
    onDisconnect(socket){
        // if no players kill lobby after some 20min
        if(!this.hasStarted){
            
            if(socket.id in this.conIds){
                let token = this.conIds[socket.id];
                this.players[token].inLobby = false;
                delete this.conIds[socket.id];
                socket.nsp.emit("playersUpdated", this.getPlayersList());
                console.log(token+" disconnected");
                this.checkReady(socket);
            }
            if(Object.keys(this.conIds).length==0){
                console.log("Last player disconnected. Killtimer started");
                this.killtimer = setTimeout(function(a){a.kill();}, 20*60*1000, this);
            }
        }
    }
    
    
    getPlayersList(){
        let list = [];
        for(let token in this.players){
            if(this.players[token].inLobby || this.players[token].ready){
                list.push(this.players[token].getStatus());
            }
        }
        return list;
    }
    
    
    onReady(token, socket){
        if(token in this.players && this.players[token].inLobby){
            this.players[token].ready = true;
            console.log(token+" is ready");
            this.checkReady(socket);
        }
    }
    
    checkReady(socket){
        for(let t in this.players){
            if(!this.players[t].ready && this.players[t].inLobby){
                socket.nsp.emit("playersUpdated", this.getPlayersList());
                return;
            }
        }
        //Everyone is ready
        this.hasStarted = true;
        socket.nsp.emit("gameStarting", "");
        this.parent.lobbyToGame(this.gamecode);
    }
    
    kill(){
        clearTimeout(this.killtimer);
        util.killNamespace(this.parent.io, '/lobby/'+this.gamecode);
        delete this.parent.activeLobbies[this.gamecode];
        console.log("Killed lobby "+this.gamecode);
    }
    
    
    //XXX debug
    debug(){
        return {};
    }
    
}
