// Player class

const util = require('./util.js');

module.exports = class {

    constructor(username){
        this.username = username;
        this.token = util.createToken(16, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
        this.usertoken = util.createToken(8, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
        this.inLobby = false;
        this.ready = false;
        this.cards = [];
    }
    
    //addCard(card){
    //    this.cards.push(card);
    //}
    
    hasCard(cardid){
        return this.cards.indexOf(cardid)>=0;
        //return this.cards.some(el=>el.id==card.id);
    }
    
    
    getStatus(){
        return {
            name: this.username,
            usertoken: this.usertoken,
            inLobby: this.inLobby,
            ready: this.ready,
            cards: this.cards.length
            };
    }

}
